package steps;


import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selectors;
import io.cucumber.java.en.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;


public class commons {

    @Given("que acesso o sistema web")
    public void que_acesso_o_sistema_web() {

        open("https://www.grocerycrud.com/v1.x/demo/bootstrap_theme");

    }

    @Given("seleciono uma nova versao")
    public void seleciono_uma_nova_versao() {

        $(Selectors.byCssSelector("#switch-version-select > option:nth-child(2)")).shouldHave(Condition.enabled).click();


    }
    @When("clico no botao adcionar consumidor")
    public void clico_no_botao_adcionar_consumidor() {

        $(Selectors.byText("Add Customer")).shouldHave(Condition.visible).click();

    }
    @When("preencho o formulario de cadastro")

    public void preencho_o_formulario_de_cadastro() {

                $("#field-customerName").setValue("Teste Sicredi");
                $("#field-contactLastName").setValue("Teste");
                $("#field-contactFirstName").setValue("Maycon Vitor");
                $("#field-phone").setValue("51 9999-9999");
                $("#field-addressLine1").setValue("Av Assis Brasil,3970");
                $("#field-addressLine2").setValue("Torre D");
                $("#field-city").setValue("Porto Alegre");
                $("#field-state").setValue("RS");
                $("#field-postalCode").setValue("91000-000");
                $("#field-country").setValue("Brasil");
                $(Selectors.byCssSelector("#field_salesRepEmployeeNumber_chosen > a > span")).click();
                $("#field-creditLimit").setValue("200");



    }
    @When("submeto cadastro do novo consumidor")
    public void submeto_cadastro_do_novo_consumidor() {
        // Write code here that turns the phrase above into concrete actions
        $(Selectors.byCssSelector("#form-button-save")).shouldBe(Condition.visible).click();


    }
    @Then("valido que usuario foi cadastrado com sucesso")
    public void valido_que_usuario_foi_cadastrado_com_sucesso() {
        // Write code here that turns the phrase above into concrete actions

        $("#report-success>p").shouldHave(Condition.text("Your data has been successfully stored into the database."));

    }



}




